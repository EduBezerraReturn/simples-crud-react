import React, { Component } from 'react';
import Lista from './Lista';

class Posts extends Component {
    render() {
       
        return (
            <div className="col-12 co-md-8">

                <Lista
                    posts={this.props.posts}
                    apagarPost={this.props.apagarPost}
                />
            </div>
        );
    }
}

export default Posts;