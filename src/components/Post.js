import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import Swal from 'sweetalert2'

class Post extends Component {
    
    confirmacaoApagar = () => {
        
        const {id} = this.props.info;

        Swal.fire({
            title: 'Quer realmente apagar?',
            text: "Você não vai poder desfazer essa ação",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Apagar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.value) {
                
                this.props.apagarPost(id);
                
                Swal.fire(
                    'Deletado',
                    'O post foi deletado.',
                    'success'
                );
            }
        });

    }

    render() {
        const {id, title} = this.props.info;
        return (
            <tr>
                <td>{id}</td>
                <td>{title}</td>
                <td>    
                    <Link to={`/post/${id}`} className="btn green">
                        Ver
                    </Link>

                    <Link to={`/editar/${id}`} className="btn whitesmoker">
                        Editar
                    </Link>

                    <button type="button" className="btn red" onClick={ this.confirmacaoApagar } >
                        Apagar
                    </button>
                </td>
            </tr>
        );
    }

   
}

export default Post;