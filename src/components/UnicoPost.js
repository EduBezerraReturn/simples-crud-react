import React, { Component } from 'react';

class UnicoPost extends Component {
    
    mostrarPost = (props) => {
        
        if(!props.post){
            return null;
        }
        const {title, body, userId} = props.post;

        return (
            <React.Fragment>
                <br></br>
                <h5> {title} </h5>
                <p>Autor: {userId} </p>
                {body}
            </React.Fragment>
        )
    }
    
    render() {
        
        
        
        return (
            <div className="col-12 col-md-8">
                {this.mostrarPost( this.props ) }
            </div>
        );
    }
}

export default UnicoPost;