import React, { Component } from 'react';
import Post from './Post';

class Lista extends Component {
    
    mostrarPosts = () => {

        const posts =  this.props.posts;
        
        if(posts.lenght === 0){
            return null;
        }

        return (
            <React.Fragment>
                {Object.keys(posts).map( (post) => (
                    <Post
                        key={post}
                        info={posts[post]}
                        apagarPost={this.props.apagarPost}
                    /> 
                ))}
            </React.Fragment>
        )
    }
    
    render() {
        return (
            <table className="table">
                <thead>
                    <tr>
                        <th scope="col"> ID </th>
                        <th scope="col"> Título </th>
                        <th scope="col"> Opções </th>
                    </tr>
                </thead>

                <tbody>
                    {this.mostrarPosts()}
                </tbody>
            </table>
        );
    }
}

export default Lista;