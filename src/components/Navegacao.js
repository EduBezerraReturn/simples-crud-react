import React from 'react';
import {Link} from 'react-router-dom';
import './Navegacao.css';


const Navegacao = () => {
    return (
        <div className="col-12 col-md-8">
            <Link to={'/'} className = "left">
                Todos os posts
            </Link>

            <Link to={'/criar'}className = "right">
                Novo post
            </Link>
        </div>
    );
};

export default Navegacao;