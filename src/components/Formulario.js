import React, { Component } from 'react';

class Formulario extends Component {

    tituloRef = React.createRef();
    conteudoRef = React.createRef();

    criarPost = (e) => {

        e.preventDefault();

       
        const post = {
            title: this.tituloRef.current.value,
            body: this.conteudoRef.current.value,
            userId:1
        }



        this.props.criarPost(post);
    }

    render() {
        return (
            <form onSubmit={this.criarPost} className="col-8">
                <legend className="text-center">Criar novo post</legend>

                <div className="form-group">
                    <label>Titulo do post</label>
                    <input ref={this.tituloRef} type="text" className="form-control" placeholder="Titulo do post" />
                </div>

                <div className="form-group">
                    <label>Conteudo</label>
                    <textarea ref={this.conteudoRef} className="form-control" placeholder="Conteudo" ></textarea>
                </div>

                <button type="submit" className="btn btn-success" > Criar </button>
            </form>
        );
    }
}

export default Formulario;
