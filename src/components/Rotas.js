import React, { Component } from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import axios from 'axios';
import Swal from 'sweetalert2'
import Header from './Header';
import Navegacao from './Navegacao';
import Posts from './Posts';
import UnicoPost from './UnicoPost';
import Formulario from './Formulario';
import Editar from './Editar';

class Router extends Component {
    
    state = {
        posts:[]
    }

    componentDidMount(){
        this.pegarPost();
    }

    pegarPost = () => {
        
        const url = `https://jsonplaceholder.typicode.com/posts`;
        
        axios.get(url)
            .then( resp => {
                this.setState({
                    posts:resp.data
                })
            } )
    }

    apagarPost = (id) => {

        const url = `https://jsonplaceholder.typicode.com/posts/${id}`;
        
        axios.delete(url)
            .then( resp => {
                
                console.log(resp); 

                if(resp.status === 200){

                    const posts =  [...this.state.posts];

                    let resultado = posts.filter( post => {
                        return post.id !== id
                    });

                    this.setState({
                        posts:resultado
                    })


                }
            } )
    }

    criarPost = (post) => {

        const url = `https://jsonplaceholder.typicode.com/posts/ `;
        
        axios.post(url, {post})
            .then( resp => {

                if(resp.status === 201){

                    Swal.fire(
                        'sucesso!',
                        'Post criado!',
                        'success'
                    );

                    const posts =  [...this.state.posts];

                    let postId = {id: resp.data.id};

                   
                    let novoPost = Object.assign( {}, resp.data.post, postId )

                    
                    this.setState( prevState => ({
                        posts: [...prevState.posts, novoPost]
                    }));


                }
            } )
    }

    editarPost = (postAtualizado) => {
        console.log(postAtualizado);

        const {id} = postAtualizado;

        const url = `https://jsonplaceholder.typicode.com/posts/${id} `;
        
        axios.put(url, {postAtualizado})
            .then( resp => {

                if(resp.status === 200){

                    let postId = resp.data.id;

                    const posts =  [...this.state.posts];
                    
                    const postEditar = posts.findIndex( post => post.id === postId );

                    posts[ postEditar ] = postAtualizado;

                    this.setState({
                        posts
                    });

                    Swal.fire(
                        'Sucesso',
                        'Post atualizado',
                        'success'
                    );

                }
            } )
    }


    
    render() {
        return (
            <BrowserRouter>

                <div className="container">
                    <div className="row justify-content-center">
                        <Header/>

                        <Navegacao/>

                        <Switch>
                            <Route exact path='/' render={ () => {
                                return (
                                    <Posts
                                        posts={this.state.posts}
                                        apagarPost={this.apagarPost}
                                    />
                                )
                            } } />

                            <Route exact path='/post/:postId' render={ (props) => {
                                
                                let idPost = props.location.pathname.replace('/post/','');

                                const posts = this.state.posts;

                                let filtro;
                                filtro = posts.filter( post => {
                                    return post.id === Number(idPost) 
                                });

                                return (
                                    <UnicoPost
                                        post = {filtro[0]}
                                    />
                                )
                            }}/>

                            <Route exact path='/criar' render={ () => {
                                return (
                                    <Formulario
                                        criarPost={this.criarPost}
                                    />
                                )
                            } } />


                            <Route exact path='/editar/:postId' render={ (props) => {
                                
                                let idPost = props.location.pathname.replace('/editar/','');

                                const posts = this.state.posts;

                                let filtro;
                                filtro = posts.filter( post => {
                                    return post.id === Number(idPost) 
                                });

                                return (
                                    <Editar
                                        post = {filtro[0]}
                                        editarPost = {this.editarPost}
                                    />
                                )
                            }}/>

                        </Switch>
                    </div>
                </div>
            
            </BrowserRouter>
        );
    }
}

export default Router;