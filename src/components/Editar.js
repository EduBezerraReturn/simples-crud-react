import React, { Component } from 'react';

class Editar extends Component {

    tituloRef = React.createRef();
    conteudoRef = React.createRef();

    editarPost = (e) => {
        
        e.preventDefault();
        
        const {id} = this.props.post;

        const post = {
            title: this.tituloRef.current.value,
            body: this.conteudoRef.current.value,
            userId:1,
            id
        }

        this.props.editarPost(post);
    }

    carregarFormulario = () => {
        
        if(!this.props.post){
            return null;
        }
        
        const {title, body} = this.props.post;

        return (
            <form onSubmit={this.editarPost} className="col-8">
                <legend className="text-center">Editar Post</legend>

                <div className="form-group">
                    <label>Titulo do Post</label>
                    <input ref={this.tituloRef} type="text" className="form-control" defaultValue={title} />
                </div>

                <div className="form-group">
                    <label>Conteudo</label>
                    <textarea ref={this.conteudoRef} className="form-control" defaultValue={body} ></textarea>
                </div>

                <button type="submit" className="btn btn-primary" > Salvar mudanças </button>
            </form>
        )
    }
    
    render() {

        
        return (
            <React.Fragment>
                { this.carregarFormulario() }
            </React.Fragment>
        );
    }
}

export default Editar;